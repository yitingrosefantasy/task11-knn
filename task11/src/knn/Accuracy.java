package knn;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import jxl.read.biff.BiffException;
import weka.core.Instance;
import weka.core.Instances;

public class Accuracy {
	//set the num of groups the train set divided to
	private static final int numOfGroup = 10;
	//set a seed to random the order of train set
	private static final long randomSeed = 8;
	
	private Instances raw;
	private int k;
	private double accuracy;
	private ArrayList<HashMap<String,Double>> similarityMatrix;
	private double[] weights;
	 
	 public Accuracy(Instances raw, int k, ArrayList<HashMap<String,Double>> similarityMatrix,
			 		double[] weights, double[] maxRange, double[] minRange) throws BiffException {
		this.k = k;
		this.raw = raw;
		this.similarityMatrix = similarityMatrix;
		this.weights = weights;
		calculate(maxRange, minRange);
	 }
	 //calculate the average accuracy
	 private void calculate(double[] maxRange, double[] minRange) throws BiffException{
		Knearest knn;
		Instances test;
		Instances train;
		Random random = new Random();
		random.setSeed(randomSeed);
		raw.randomize(random);
		int error = 0;
		for(int i = 0; i < numOfGroup; i++) {
			// get the train data set
			train = this.raw.trainCV(numOfGroup, i);
			// get the test data set
			test = this.raw.testCV(numOfGroup, i);
			Instances copy = new Instances(test);
			knn = new Knearest(train, test, k, similarityMatrix, weights, maxRange, minRange);
			knn.compute();
			
			for(int j = 0; j < knn.getResult().size(); j++) {
		
				 if(!knn.getResult().get(j).stringValue(train.numAttributes() - 1)
						 .equals(copy.get(j).stringValue(train.numAttributes() - 1))) {
					 error++;

				 }
			}
		}
		//System.out.println(error);
		this.accuracy = 1 - (double)(error) / (double)(this.raw.size()); 
	 }
	 
	 public double getAccuracy(){
		 return this.accuracy;
	 }
	 
}
