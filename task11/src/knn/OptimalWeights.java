package knn;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import jxl.read.biff.BiffException;
import weka.core.Instances;

public class OptimalWeights {

	private int num;
	private double[] optimalWeights;
	private double accuracy = 0;
	// target accuracy want to achieve
	private static final double targetAccuracy = 0.96;
	public OptimalWeights(Instances train, int k, 
						ArrayList<HashMap<String,Double>> similarityMatrix, 
						double[] maxRange, double[] minRange) throws BiffException {
		this.num = train.numAttributes() - 1;
		optimalWeights = new double[num];
		do {
			randomWeights();
			Accuracy crossValidation = new Accuracy(train, k, similarityMatrix, optimalWeights, maxRange, minRange);
			
			accuracy = crossValidation.getAccuracy();
		} while (accuracy < targetAccuracy); //random weights until the accuracy is above 90%
		
		
		System.out.println(Arrays.toString(optimalWeights));
		System.out.print(accuracy);
		System.out.println();
	}
	private void randomWeights() {
		for(int i = 0; i < num; i++) {
			this.optimalWeights[i] = Math.random() * (i + 2);
		}
	}
	
	public double[] getWeights() {
		return this.optimalWeights;
	}
	
	public double getAccuracy() {
		return this.accuracy;
	}
}
