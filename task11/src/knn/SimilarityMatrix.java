package knn;

import java.util.ArrayList;
import java.util.HashMap;
import java.io.*;

import jxl.*;
import jxl.read.biff.BiffException;

public class SimilarityMatrix {
	private ArrayList<HashMap<String,Double>> matrixA = new ArrayList<HashMap<String,Double>>();
	private ArrayList<HashMap<String,Double>> matrixB = new ArrayList<HashMap<String,Double>>();
	// assume every task is an independent excel file and every similarity matix is in one sheet.
	
	public SimilarityMatrix() throws BiffException {
			try {
				Workbook bookA = Workbook.getWorkbook(new File("similaritymatrixA.xls"));
				Workbook bookB = Workbook.getWorkbook(new File("similaritymatrixB.xls"));
				this.matrixA = helper(bookA);
				this.matrixB = helper(bookB);
				
				
				bookA.close();
				bookB.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			 
		
	}
	
	private ArrayList<HashMap<String,Double>> helper(Workbook book) {
		ArrayList<HashMap<String,Double>> result = new ArrayList<HashMap<String,Double>>();
		//System.out.println(book.getNumberOfSheets());
		for(int i = 0; i < book.getNumberOfSheets(); i++) {
			HashMap<String,Double> matrix = new HashMap<String,Double>();
			Sheet sheet = book.getSheet(i);
			for(int j = 1; j < sheet.getColumns(); j++) {
				for(int m = 1; m < sheet.getRows(); m++) {
					// int key could work faster
					matrix.put(String.valueOf(j - 1)
									+ String.valueOf(m - 1),
									Double.parseDouble(sheet.getCell(j, m).getContents())); 
			
				}
			}
			result.add(matrix);
			//System.out.println(matrix.toString());
		}
		return result;
	}
	
	public ArrayList<HashMap<String,Double>> getMatrix(String name) {
		if(name.equals("A")) return matrixA;
		if(name.equals("B")) return matrixB;
		return null;
	}
}
