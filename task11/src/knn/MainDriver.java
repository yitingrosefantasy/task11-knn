package knn;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import jxl.read.biff.BiffException;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ArffLoader;
public class MainDriver {
	
	 private static final String ARFF_TRAIN_PATHA = "trainProdSelection.arff";
	 private static final String ARFF_TEST_PATHA = "testProdSelection.arff";
	 private static final String ARFF_TRAIN_PATHB = "trainProdIntro.binary.arff";
	 private static final String ARFF_TEST_PATHB = "testProdIntro.binary.arff";
	 private static final String ARFF_TRAIN_PATHREAL = "trainProdIntro.real.arff";
	 private static final String ARFF_TEST_PATHREAL = "testProdIntro.real.arff";
	 
	 private static ArrayList<HashMap<String,Double>> similarityMatrixA;
	 private static ArrayList<HashMap<String,Double>> similarityMatrixB;
	 
	//add the arrange of all attributes, for discrete attribute make it 0
	private static final double[] maxRangeA = {0, 0, 60.0, 3000, 40.0, 20.0};
	private static final double[] minRangeA = {0, 0, 0.0, 5, 10.0, 0.0};
	private static final double[] maxRangeB = {0, 0, 20.0, 10, 0, 0, 100, 120};
	private static final double[] minRangeB = {0, 0, 0.5, 0, 0, 0, 0, 0};
	 private static final int k = 5;
	 
	 
	 public static void main(String[] args) throws IOException {
	     // just change   
	     Instances trainA = getData(ARFF_TRAIN_PATHA);
	     Instances testA = getData(ARFF_TEST_PATHA);
	     Instances trainB = getData(ARFF_TRAIN_PATHB);
	     Instances testB = getData(ARFF_TEST_PATHB);
	     Instances trainR = getData(ARFF_TRAIN_PATHREAL);
	     Instances testR = getData(ARFF_TEST_PATHREAL);

	    
	     try {
	    	 SimilarityMatrix matrix = new SimilarityMatrix();
		     similarityMatrixA = matrix.getMatrix("A");
		     similarityMatrixB = matrix.getMatrix("B");
		     // if wanna optimal weights umcomment the line below
		     //OptimalWeights weights = new OptimalWeights(trainB, k, similarityMatrixB, maxRangeB, minRangeB);
		     
		     // you can also manually add weights
		     double[] weights = {0.88, 0.96, 1.59, 4.49, 1.85, 1.28, 6.94, 5.19};
		     
		     // if wanna do cross validation, uncomment the line below.
		     //Accuracy accuracy = new Accuracy(trainB, k, similarityMatrixB, weights, maxRangeB, minRangeB);
		     Knearest knn = new Knearest(trainR, testR, k, similarityMatrixB, weights, maxRangeB, minRangeB);
			knn.compute();
			
	    	System.out.println(knn.getResult()); 
		    //System.out.print(accuracy.getAccuracy());
			
		} catch (BiffException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	     
	 }
	 //getData from arff files
	 private static Instances getData(String fileName) throws IOException {
		 ArffLoader arffLoader = new ArffLoader();
	     File datasetFile = new File(fileName);
	     arffLoader.setFile(datasetFile);
	     Instances dataInstances = arffLoader.getDataSet();
	     return dataInstances;
	 }
     
}