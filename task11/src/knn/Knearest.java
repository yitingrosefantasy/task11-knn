package knn;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.Map.Entry;

import jxl.read.biff.BiffException;
import weka.core.Instance;
import weka.core.Instances;
public class Knearest {
	
	private ArrayList<HashMap<String,Double>> similarityMatrix;
	private double[] max;
	private double[] min;
	private Instances train;
	private Instances test;
	private int k;
	private int numOfAttribute;
	private double[] weights = new double[numOfAttribute];
	
	public Knearest(Instances train, Instances test, int k, 
					ArrayList<HashMap<String,Double>> similarityMatrix, 
					double[] weights, double[] maxRange, double[] minRange) {
		this.test = test;
		this.train = train;
		this.k = k;
		this.similarityMatrix = similarityMatrix;
		this.weights = weights;
		this.numOfAttribute = train.numAttributes() - 1;
		this.max = new double[numOfAttribute];
		this.min = new double[numOfAttribute];
		getMinAndMax(maxRange, minRange);
	}
	
	public void compute() throws BiffException {

		for(Instance inst : this.test) {
			HashMap<Instance, Double> distances = new HashMap<Instance, Double>();
			for(Instance neighbor : this.train) {
				//the count of string attributes
				int count = 0;
				
				double distance = 0;
				double sum = 0;
				for(int i = 0; i < numOfAttribute; i++) {
					double var = 0;
					
					if(!this.train.attribute(i).isNumeric()) { //if the attribute is string type
						var = 1 - this.similarityMatrix.get(count).get(String.valueOf((int)inst.value(i)) +
								String.valueOf((int) neighbor.value(i)));
						var *= weights[i];
						count++;
						//System.out.println((int) inst.value(i));
					} else {
						var = Math.pow(normalize(neighbor.value(i), i) - normalize(inst.value(i), i), 2);
						var *= weights[i];
					}
					sum +=var;	
				}
				distance = Math.sqrt(sum);
//				
				//store distance with every neighbor and the instance in the arraylist
				distances.put(neighbor, distance);
			}
			
			// sort the map by key-distance value
			Set<Entry<Instance, Double>> set = distances.entrySet();
			List<Entry<Instance, Double>> list = new ArrayList<Entry<Instance, Double>>(
					set);
			Collections.sort(list, new Comparator<Map.Entry<Instance, Double>>() {
				public int compare(Map.Entry<Instance, Double> o1,
						Map.Entry<Instance, Double> o2) {
					// if ditance are identical, compare the class index
					if(o1.getValue().compareTo(o2.getValue()) == 0) {
						return (o1.getKey().stringValue(numOfAttribute)
								.compareTo(o2.getKey().stringValue(numOfAttribute)));
					}
						
					return (o1.getValue()).compareTo(o2.getValue());
				}
			});
			
			// if the last column is numeric
			if (this.train.attribute(numOfAttribute).isNumeric()) {
				int sum = 0;
				for (int i = 0; i < k; i++) {
					sum += list.get(i).getKey().value(numOfAttribute);
				}
				inst.setValue(numOfAttribute, (double)sum / (double)k);
			} else {
				// get the first k neighbor, key is the class, if there are two identify key,
				// it mean there are more than one class in top k, then replace the value.
				HashMap<String, Double> classes = new HashMap<String, Double>();
				for(int i = 0; i < k; i++) {
					if(classes.get(list.get(i).getKey().stringValue(numOfAttribute)) != null) {
						classes.replace(list.get(i).getKey().stringValue(numOfAttribute),
								// invert distance
										1.00 / list.get(i).getValue() 
										+ classes.get(list.get(i).getKey().stringValue(numOfAttribute)));
						
					} else {
						classes.put(list.get(i).getKey().stringValue(numOfAttribute), 1.00 / list.get(i).getValue());
					}
					
				}
				
				//sort the weigh of class label
				Set<Entry<String, Double>> set2 = classes.entrySet();
				List<Entry<String, Double>> list2 = new ArrayList<Entry<String, Double>>(
						set2);
				
				//print out scores
//				double sum = 0;
//				for(int i = 0; i < list2.size(); i++) {
//					sum += list2.get(i).getValue();
//				}
//				for(int i = 0; i < list2.size(); i++) {
//					System.out.print(list2.get(i).getValue() / sum + "  ");
//				}
//				System.out.println();
				
				Collections.sort(list2, new MyComparator());
				
				// get the class with largest weight
				inst.setValue(numOfAttribute, list2.get(0).getKey());
				// index store in the key, get the instance of the train and get the class of this instance.
			}
		}
	
	}
	
	//get the max and min value of each attribute
	private void getMinAndMax(double[] maxRange, double[] minRange) {
		Instances instances = new Instances(this.train);
		 for(int i = 0; i < numOfAttribute; i++) {
			 // although symbolic attribute do not need to nomalize,
			 // it will need be invoked, just stay in the array
			 instances.stableSort(i);
			 double min = instances.firstInstance().value(i);
			 double max = instances.lastInstance().value(i);
			 // in case the data is not in the range
			 this.min[i] = Math.max(min, minRange[i]);
			 this.max[i] = Math.min(max, maxRange[i]);
			 //System.out.println(this.min[i]);
			 //System.out.println(this.max[i]);
		 }

	}
	
	//normalize numeric attributes
	private double normalize(double value, int index) {
		return (value - this.min[index]) / (this.max[index] - this.min[index]);
	}
	
	//Comparator
	private static class MyComparator implements Comparator<Map.Entry<String, Double>> {
		public int compare(Map.Entry<String, Double> o1,
				Map.Entry<String, Double> o2) {
			if(o2.getValue().compareTo(o1.getValue()) == 0)
				return o1.getKey().compareTo(o2.getKey());
			
			// get the biggest distance
			return (o2.getValue()).compareTo(o1.getValue());
		}
	}
	
	public Instances getResult() {
		return this.test;
	}
	
	
}